# /usr/bin/python3
#-*- coding: utf-8-*-
#
# count-by-group.py [-s gid|gname|nusers] -u users -g groups 
#  
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
        """LListem els usuaris per file""",\
        epilog="Forza Horizon 4")
parser.add_argument("-u","--userFile",type=str,\
        help="User file /etc/passwd style",metavar="userFile",\
        required=True)
parser.add_argument("-g","--groupFile",type=str,\
        help="Group file /etc/group style",metavar="groupFile",\
        required=True)

parser.add_argument("-s","--sort",type=str,\
        help="Ordena per login|gid",metavar="criteri",\
        choices=["login","gid","gname"],dest="criteri",default="login")
args=parser.parse_args()
# -------------------------------------
class UnixUser():
  "Classe UnixUser: prototipus de /etc/passwd login:passwd:uid:gid:gecos:home:shell"
  def __init__(self, userLine):	
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    if self.gid in groupDict:
        self.gname=groupDict[self.gid].gname
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]
  
  def show(self):
    "Mostra les dades de l'usuari"
    print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")
  def __str__(self):
    "functió to_string"
    return "%s %s %d %d %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)

#-------------------------------------------
class UnixGroup():
    "Classe UnixGroup: tipus /etc/group gname:passwd:gid:userList"
    def __init__(self, groupLine):
        "Constructor d'objectes UnixGroup"
        groupField=groupLine.split(":")
        self.gname=groupField[0]
        self.passwd=groupField[1]
        self.gid=int(groupField[2])
        self.userListStr=groupField[3][:-1]
        self.userList=[]
        if self.userListStr:
            self.userList = self.userListStr.split(",")

    def __str__(self):
        "functió to_string d'un objecte UnixGroup"
        return "%s %d %s" % (self.gname, int(self.gid), self.userList)


#-------------------------------------------
def ordena_login(usuari):
    "Metode que ordena per login els usuaris"
    return (usuari.login)
  
def ordena_gid(usuari):
    "Metode que ordena per gid els usuaris"
    return (usuari.gid, usuari.login)

def ordena_gname(user):
  '''Comparador d'usuaris segons el gname'''
  return (usuari.gname, usuari.login)

print("Programa")
#-------------------------
groupDict={}
groupFile=open(args.groupFile,"r")
for line in groupFile:
    group=UnixGroup(line)
    groupDict[group.gid]=group
groupFile.close()
#------------------------
userFile=open(args.userFile,"r")
userList=[]
for line in userFile:
    usuari=UnixUser(line)
    userList.append(usuari)
userFile.close()

if args.criteri=="login":
    userList.sort(key=ordena_login)
elif args.criteri=="gid":
    userList.sort(key=ordena_gid)
else:
    userList.sort(key=ordena_gname)

for user in userList:
 print(user, end=" ")
exit(0)

# /usr/bin/python
#-*- coding: utf-8-*-
# telnet-server.py [-p PORT]  
# -------------------------------------
# Jamison Quelal ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE
#----------------------------------------
parser = argparse.ArgumentParser(description="""Telnet server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
#---------------------------------------
HOST = ''
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

while True:
    conn, addr = s.accept()
    print("Connectat per: ", addr)
    while True:
        command = conn.recv(1024)
        if not command:
            print("S\'ha tancat la connexio amb: ", addr)
            conn.close()
            break
        pipeData = Popen(command,shell=True,stdout=PIPE)
        for line in pipeData.stdout:
            conn.send(line)
        conn.send(MYEOF)
    
    conn.close()

sys.close(0)


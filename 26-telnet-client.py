# /usr/bin/python3
#-*- coding: utf-8-*-
# telnet-client.py -p port -s server
#-----------------------------------
# Jamison Quelal ASIX M06 Curs 2021-2022
# Febrer 2022
#-------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE
#-------------------------------------
parser = argparse.ArgumentParser(description=\
        """Client Telnet""")
parser.add_argument("server",type=str)
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
#---------------------------------------
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

while True:
    command = input("telnet>")
    if command == 'exit':
        s.close()
        break
    s.send(command.encode())
    while True:
        data = s.recv(1024)
        print(str(data))
        if data[-1:] == b'\x04':
            break
print("Hem Sortit")
sys.exit(0)

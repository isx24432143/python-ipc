# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 
#  
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
        """LListem els usuaris per file""",\
        epilog="Forza Horizon 4")
parser.add_argument("file",type=str,\
        help="User file /etc/passwd style",metavar="file")
parser.add_argument("-s","--sort",type=str,\
        help="Ordena per login|gid",metavar="criteri",\
        choices=["login","gid"],dest="criteri",default="login")
args=parser.parse_args()
# -------------------------------------
class UnixUser():
  "Classe UnixUser: prototipus de /etc/passwd login:passwd:uid:gid:gecos:home:shell"
  def __init__(self, userLine):	
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6][:-1]
  
  def show(self):
    "Mostra les dades de l'usuari"
    print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")
  def __str__(self):
    "functió to_string"
    return "%s %s %d %d %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
  
def ordena_login(usuari):
    "Metode que ordena per login els usuaris"
    return (usuari.login)
  
def ordena_gid(usuari):
    "Metode que ordena per gid els usuaris"
    return (usuari.gid)

print("Programa")
fileIn=open(args.file,"r")
userList=[]
for line in fileIn:
    usuari=UnixUser(line)
    userList.append(usuari)
fileIn.close()

if args.criteri=="login":
    userList.sort(key=ordena_login)
else:
    userList.sort(key=ordena_gid)

for user in userList:
 print(user)
exit(0)

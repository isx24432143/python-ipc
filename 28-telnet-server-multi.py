#!/usr/bin/python
#-*- coding: utf-8-*-
# Escola del treball de Barcelona
# Jamison Quelal Curs 2021-2022
# Febrer 2022
# Echo server multiple-connexions
# -----------------------------------------------------------------
import socket, sys, select, os
from subprocess import Popen, PIPE
#------------------------------------------------------------------
HOST = ''
PORT = 50007
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)

print(os.getpid())
conns=[s]
MYEOF = bytes(chr(4), 'utf-8')
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
        if actual == s:
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else:
            command = actual.recv(1024)
            if not command:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
                break
            else:
                command = command.decode()
                pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
                for line in pipeData.stderr:
                    actual.send(line)
                for line in pipeData.stdout:
                    actual.send(line)
                actual.send(MYEOF)

s.close()
sys.exit(0)

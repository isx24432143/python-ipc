# /usr/bin/python3
#-*- coding: utf-8-*-
#
# daytime-client.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect((HOST, PORT))
while True:
    data = s.recv(1024)
    if not data: break
    print("La data del dia d'avui es la seguent: ", repr(data))
s.close()
sys.exit(0)

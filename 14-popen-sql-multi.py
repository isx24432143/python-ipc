# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Consulata PSQL""")
parser.add_argument("-c","--client",type=str,\
        help="Clients",action="append",dest="clientList",required=True)
parser.add_argument("-d","--database", help="Database",default="training")
args=parser.parse_args()
# -------------------------------------------------------
command = "psql -qtA -F',' "+args.database
pipeData = Popen(command,shell=True,bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for num_clie in args.clientList:
    sqlConsulta="SELECT * FROM cliente WHERE cliecod="+str(num_clie)+";"    
    pipeData.stdin.write(sqlConsulta+"\n")    
    print(pipeData.stdout.readline(), end="")

pipeData.stdin.write("\q\n")
exit(0)

# /usr/bin/python3
#-*- coding: utf-8-*-
#
# signal-exemple.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, os, signal, argparse
parser = argparse.ArgumentParser(description="Gestionar l'alarma")
parser.add_argument("segons", type=int, help="segons")
args=parser.parse_args()
global mes
global menys
mes = 0
menys = 0

def usr1custom(signum,frame):
    global mes
    print("Signal handler called with signal: ", signum)
    ara=signal.alarm(0)
    mes+=1
    signal.alarm(ara+60)

def usr2custom(signum,frame):
    global menys
    print("Signal handler called with signal: ", signum)
    ara=signal.alarm(0)
    menys+=1
    if ara-60<0: 
        print("S'ha ignorat %d" % (ara))
        signal.alarm(ara)
    else:
        signal.alarm(ara-60)

def myterm(signum,frame):
    print("Signal handler called with signal: ", signum)
    queda=signal.alarm(0)
    signal.alarm(queda)
    print("Queden "+str(queda)+" segons")

def myhupcustom(signum,frame):
    print("Signal handler called with signal: ", signum)
    print("El temps s'ha reiniciat a: "+str(args.segons)+" segons")
    signal.alarm(args.segons)

def myalarmcustom(signum,frame):
    global mes, meys
    print("Signal handler called with signal: ", signum)
    print("Acabant l'alarma. Comptador: Apujar: "+str(mes)+" i Reduir: "+str(menys))
    sys.exit(1)


signal.signal(signal.SIGHUP,myhupcustom) # 1
signal.signal(signal.SIGUSR1,usr1custom) # 10
signal.signal(signal.SIGUSR2,usr2custom)   # 12
signal.signal(signal.SIGALRM,myalarmcustom) # 14
signal.signal(signal.SIGTERM,myterm) #15
signal.signal(signal.SIGINT,signal.SIG_IGN)

signal.alarm(args.segons)
print(os.getpid())
while True:
  pass
signal.alarm(0)
sys.exit(0)

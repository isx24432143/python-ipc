# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Consulata PSQL""")
parser.add_argument("sqlConsulta",type=str,\
        help="Consulta interactiva")
args=parser.parse_args()
# -------------------------------------------------------
command = "psql -qtA -F',' training"
pipeData = Popen(command,shell=True,bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(args.sqlConsulta+"\n\q\n")
for line in pipeData.stdout:
    print(line, end="")
exit(0)
